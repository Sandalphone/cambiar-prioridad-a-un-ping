#!/bin/bash

#Alberto Saavedra
#PBSI-14g
#Administracion y Seguridad en Linux
#Comando Original
renice -n 10 -p $(ps -aux | grep ping | tr -s '[:space:]' | cut -d ' ' -f 2 | head -n1)

#propuesta quitando comandos
renice -n 10 -p $(ps -a | grep ping | cut -d ' ' -f2)

#propuesta utilizando awk
renice -n 10 $(ps -aux | awk '$11~"ping"{print $2}')
